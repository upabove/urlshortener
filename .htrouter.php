<?php

define('PUBLIC_PATH', __DIR__ . '/public');
$uri = urldecode(parse_url($_SERVER['REQUEST_URI'], PHP_URL_PATH));
if ($uri !== '/' && file_exists(PUBLIC_PATH . $uri)) {
    return false;
}
$_GET['_url'] = $_SERVER['REQUEST_URI'];
require_once PUBLIC_PATH . '/index.php';
