<!DOCTYPE html>
<html>
    <head>
        <title>Phalcon URL Shortener by Mateusz Pająk</title>
        <?= $this->tag->stylesheetLink('css/bootstrap.min.css') ?>
        <?= $this->tag->stylesheetLink('css/main.css') ?>
    </head>
    <body>
      <div class="container">
        <div class="row">
          <div class="col-12">
            <header id="header-main" class="text-center">
              <h1><a href="<?= $this->url->get('') ?>"><strong>URL</strong> Shortener</a></h1>
              <h6>It makes them short!</h6>
              <hr />
            </header>
          </div>
        </div>
        <div class="row">
          <div class="col-12">
            <form method="post" action="short/">
              <div class="form-group">
                <label for="url">Some long URL:</label>
                <input type="text" class="form-control" id="url" name="url">
              </div>
              <div class="form-group">
                <button type="submit" class="btn btn-primary">Send</button>
              </div>
            </form>
          </div>
        </div>
        <div class="row">
          <div class="col-12">
            <footer id="footer-main" class="text-center">
              <hr />
              <h6>Made with Phalcon &hearts; by <a href="https://mphp.pl">Mateusz Pająk</a></h6>
            </footer>
          </div>
        </div>
      </div>
    </body>
</html>
