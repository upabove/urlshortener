<!DOCTYPE html>
<html>
    <head>
        <title>Phalcon URL Shortener by Mateusz Pająk</title>
        {{ stylesheet_link("css/bootstrap.min.css") }}
        {{ stylesheet_link("css/main.css") }}
    </head>
    <body>
      <div class="container">
        <div class="row">
          <div class="col-12">
            <header id="header-main" class="text-center">
              <h1><a href="{{ url('') }}"><strong>URL</strong> Shortener</a></h1>
              <h6>Your URL is short now!</h6>
              <hr />
            </header>
          </div>
        </div>
        <div class="row">
          <div class="col-12">
            <div class="form-group">
              Your old, long URL:<br/>
              <a href="{{ urlLong }}" title="Your old URL">{{ urlLong }}</a>
            </div>
          </div>
        </div>
        <div class="row">
          <div class="col-12">
            <form>
              <div class="form-group">
                <label for="url">Your new, short URL:</label>
                <input type="text" class="form-control" id="url" name="url" onFocus="this.select()" value="{{ url('go/') }}{{ urlName }}">
              </div>
            </form>
          </div>
        </div>
        <div class="row">
          <div class="col-12">
            <footer id="footer-main" class="text-center">
              <hr />
              <h6>Made with Phalcon &hearts; by <a href="https://mphp.pl">Mateusz Pająk</a></h6>
            </footer>
          </div>
        </div>
      </div>
    </body>
</html>
