<?php

$router = new Phalcon\Mvc\Router(false);

// Define a route
$router->add(
    '/',
    [
        'controller' => 'index',
        'action'     => 'index',
    ]
);

$router->add(
  '/go/{name}',
  [
    'controller'  => 'go',
    'action'      => 'redirect'
  ]
);

$router->add(
  '/short/{name}',
  [
    'controller'  => 'short',
    'action'      => 'index'
  ]
);

return $router;
