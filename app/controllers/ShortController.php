<?php

use Phalcon\Http\Response;
use Phalcon\Mvc\Controller;

class ShortController extends Controller
{
  public function indexAction()
  {

    if ($this->request->isPost()) {

      $url = $this->request->getPost("url");

      if ($url === "") {
        return $this->response->redirect("");
      }

      // If Url Exist
      $urlExist = Urls::findFirst(
        [
          "url = :url:",
          "bind" => [
            "url" => $url
          ]
        ]
      );

      if ($urlExist != false) {

        $urlName = $urlExist->name;
        $urlUrl = $urlExist->url;

      } else {

        $lastUrl = Urls::findFirst(
          [
            'order' => 'id DESC'
          ]
        );

        // If DB is empty
        $UrlId = 0;
        if ($lastUrl != false) {
          $UrlId = $lastUrl->id+1;
        } else {
          $UrlId = 1;
        }

        // Save Url
        $urlObj = new Urls();

        $urlObj->name = base64_encode($UrlId);
        $urlObj->url = $url;
        $urlObj->date = date("j F Y");

        $urlObj->save();

        $urlName = $urlObj->name;
        $urlUrl = $urlObj->url;
      }

      // Display result
      $this->view->urlName = $urlName;
      $this->view->urlLong = $urlUrl;

    } else {
      return $this->response->redirect("");
    }

  }

}
