<?php

use Phalcon\Http\Response;
use Phalcon\Mvc\Controller;

class GoController extends Controller
{
  public function indexAction()
  {

  }

  public function redirectAction()
  {

    $response = new Response();

    $name = $this->dispatcher->getParam('name');

    // Check if url exist
    $urlExist = Urls::findFirst(
      [
        "name = :name:",
        "bind" => [
          "name" => $name
        ]
      ]
    );

    if ($urlExist != false) {

      return $response->redirect($urlExist->url, true);

    } else {
      return $response->redirect("");
    }

  }
}
