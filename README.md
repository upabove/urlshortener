# urlshortener
Basic tool for making short URLs. Built for portfolio with Phalcon PHP framework.

Online version: https://test.mphp.pl/urlshortener/

Author:
Mateusz Pająk
https://mphp.pl